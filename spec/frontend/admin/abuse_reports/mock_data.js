export const mockAbuseReports = [
  {
    category: 'spam',
    updatedAt: '2022-12-07T06:45:39.977Z',
    reporter: { name: 'Ms. Admin' },
    reportedUser: { name: 'Mr. Abuser' },
    reportedUserPath: '/mr_abuser',
    reporterPath: '/admin',
  },
  {
    category: 'phishing',
    updatedAt: '2022-12-07T06:45:39.977Z',
    reporter: { name: 'Ms. Reporter' },
    reportedUser: { name: 'Mr. Phisher' },
    reportedUserPath: '/mr_phisher',
    reporterPath: '/admin',
  },
];
